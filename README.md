# Amazon Scraper

A [Scrapy](https://scrapy.org/) project to gather information from Amazon. Until now, we have these spiders:
  - SubscribeAndSaveSpider - products from Subscribe and Save categories. 

Each spider is responsible to gather items from different parts of Amazon.

### Before use
  - Install [GIT](https://git-scm.com/downloads)
  - Install [Python 3.7.3](https://www.python.org/downloads/release/python-373/). 
    - Do not forget to add python on your PATH.
    - Other versions of Python 3 can be used.
  - Download the code:
    - Open a terminal, select a working directory and type:
        ```
        $ git clone https://gitlab.com/upfreelas/amazonscraper
        ```
    - A folder called amazonscraper will be created with all project files. Enter on the folder:
        ```
        $ cd amazonscraper
        ```
    - Install the project dependencies:
        ```
        $ pip install -r requirements.txt
        ```
  - You have the code ready to use!
  - Hint! Always that you want to use the code, check if this repository did not update:
    ```
    $ cd amazonscraper
    $ git pull
    ```
     - GIT will pull the code from here if it detects any changes.
     
### Usage
  - To see which spiders are available for use, type:
    ```
    $ scrapy list
    ```
  - Run a spider:
    ```
    $ scrapy crawl subscribe_and_save
    ```
  - Many types of arguments can be used to customize the crawling process. See [Scrapy documentation](https://docs.scrapy.org/en/latest/intro/tutorial.html#using-spider-arguments).

### Hints
  - Avoid running all URLs on the same spider instance. Prefer to split an instance per category.
  - This approach permits almost a "scrape" in sequence and it does not compromise the spider's speed.
  - For example, to scrape each category, you can follow the commands below:

###### Household
    scrapy crawl subscribe_and_save -o household.csv -a urls="https://www.amazon.com/s?k=household+supplies&srs=5856181011&rh=n%3A5856181011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_2a1_w"

###### Baby Products
    scrapy crawl subscribe_and_save -o baby.csv -a urls="https://www.amazon.com/s?i=baby&bbn=5856181011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_2b1_w"

###### Pet Supplies
    scrapy crawl subscribe_and_save -o pet.csv -a urls="https://www.amazon.com/s?i=pets&bbn=5856181011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_2c1_w"

###### Beauty
    scrapy crawl subscribe_and_save -o beauty.csv -a urls="https://www.amazon.com/s?bbn=3760911&rh=n%3A3760911%2Cp_n_is_sns_available%3A2617006011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_3c1_w"

###### Health Care
    scrapy crawl subscribe_and_save -o health.csv -a urls="https://www.amazon.com/Medications-Treatments-Subscribe-Save-Eligible/s?i=hpc&bbn=3760941&rh=n%3A3760901%2Cn%3A%213760931%2Cn%3A3760941%2Cp_n_is_sns_available%3A2617006011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_3b1_w"

###### Grocery
    scrapy crawl subscribe_and_save -o grocery.csv -a urls="https://www.amazon.com/s?i=grocery&bbn=5856181011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_3a1_w"

  - Every time a spider reaches the final page of a category, it will open this last page in a browser. Take a look to verify if it is ok.