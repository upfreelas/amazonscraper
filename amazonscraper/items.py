# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from re import search
from w3lib.html import remove_tags
from scrapy.loader.processors import MapCompose, TakeFirst, Join


def normalize_space(s):
    """Normalizes spaces in fields.
    """
    return " ".join(s.split())
        
def remove_empty(s):
    """Returns None if string is empty, otherwise string itself
    """
    return s if s else None

def remove_breaklines(s):
    """Removes breaklines from string
    """
    return s.replace('\n', ' ').replace('\r', '')

def strip_space(s):
    """Strip spaces on strings.
    """
    return s.strip()

def encode_UTF8(s):
    """Encodes string to UTF-8
    """
    return s.encode('utf-8')

def split_by_comma(s):
    """Splits string using comma.
    """
    return s.split(',')

def remove_strange_characters(s):
    s = s.replace('&amp;', '&')
    return s

def format_category(s):
    categories = ['Grocery & Gourmet Food',
                  'Health Care',
                  'Beauty & Personal Care',
                  'Pet Supplies',
                  'Baby Products',
                  'household supplies']
    for category in categories:
        if search(category, s):
            return category
    return ''

class Product(scrapy.Item):
    title = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags, strip_space,
                                                    remove_strange_characters),
                         output_processor=TakeFirst())
    brand = scrapy.Field(input_processor=MapCompose(normalize_space,remove_tags, strip_space,
                                                    remove_strange_characters),
                      output_processor=TakeFirst())
    reviews = scrapy.Field(input_processor=MapCompose(normalize_space,remove_tags, strip_space),
                           output_processor=TakeFirst())
    reviews_qty = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags, strip_space),
                               output_processor=TakeFirst())
    bsr_position = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags, strip_space),
                                output_processor=TakeFirst())
    bsr_category = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags, strip_space,
                                                           remove_strange_characters),
                                output_processor=TakeFirst())
    requested_url = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags, strip_space),
                                 output_processor=TakeFirst())
    image = scrapy.Field(output_processor=TakeFirst())
    sold_by = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags, strip_space,
                                                      remove_strange_characters),
                           output_processor=TakeFirst())
    _type = scrapy.Field(output_processor=TakeFirst())
    asin = scrapy.Field(output_processor=TakeFirst())
    price = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags, strip_space),
                         output_processor=TakeFirst())
    canonical_url = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags, strip_space),
                                 output_processor=TakeFirst())
    keywords = scrapy.Field(input_processor=MapCompose(split_by_comma, strip_space,
                                                       remove_strange_characters))
    page = scrapy.Field(input_processor=MapCompose(remove_tags), output_processor=TakeFirst())
    location = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags, strip_space),
                            output_processor=TakeFirst())
    referrered_url = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags, strip_space),
                                  output_processor=TakeFirst())
    subcategories = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags, strip_space,
                                                            remove_strange_characters))
    position = scrapy.Field(output_processor=TakeFirst())
    category = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags, strip_space,
                                                       remove_strange_characters, format_category),
                            output_processor=TakeFirst())