# -*- coding: utf-8 -*-
import scrapy
import urllib.parse as urlparse
from scrapy.http import Request
from scrapy.loader import ItemLoader
from amazonscraper.items import Product
from scrapy.utils.response import open_in_browser
from scrapy.utils.project import get_project_settings


def prepare_request(response, callback, meta_to_reload=None):
    """Prepares a request based on response and callback function.

    Parameters
    ----------
    response : response
        A response used to prepare the request.
    callback : function
        A function where the request will be send.
    meta_to_reload : list
        A list with meta keys to reload on request.
    Return
    ------
    request : scrapy.http.Request
        The prepared request.
    """
    request = Request(response.url, callback=callback, dont_filter=True)
    # Update attempts value
    if response.meta.get('attempts'):
        request.meta['attempts'] = int(response.meta.get('attempts')) + 1
    else:
        request.meta['attempts'] = 1

    # Load meta on request again
    for k, v in response.meta.items():
        if meta_to_reload and k in meta_to_reload:
            request.meta[k] = v
    
    return request

class SubscribeAndSaveSpider(scrapy.Spider):
    name = 'subscribe_and_save'
    allowed_domains = ['amazon.com', 'api.proxycrawl.com']
    default_urls = ['https://www.amazon.com/s?k=household+supplies&srs=5856181011&rh=n%3A5856181011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_2a1_w',
                    'https://www.amazon.com/s?i=baby&bbn=5856181011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_2b1_w',
                    'https://www.amazon.com/s?i=pets&bbn=5856181011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_2c1_w',
                    'https://www.amazon.com/s?bbn=3760911&rh=n%3A3760911%2Cp_n_is_sns_available%3A2617006011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_3c1_w',
                    'https://www.amazon.com/Medications-Treatments-Subscribe-Save-Eligible/s?i=hpc&bbn=3760941&rh=n%3A3760901%2Cn%3A%213760931%2Cn%3A3760941%2Cp_n_is_sns_available%3A2617006011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_3b1_w',
                    'https://www.amazon.com/s?i=grocery&bbn=5856181011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_3a1_w']
    custom_settings = {
        'FEED_EXPORT_FIELDS' : ['_type', 'asin', 'category', 'page', 'position', 'subcategories', 'bsr_category', 'bsr_position', 'price',
                                'brand', 'reviews', 'reviews_qty', 'sold_by', 'title' , 'keywords',  'canonical_url', 'image', 'location',
                                'requested_url', 'referrered_url'],
        'LOG_LEVEL' : 'INFO',
        'CONCURRENT_REQUESTS' : 4,
        'DEPTH_PRIORITY' : 1,
        'SCHEDULER_DISK_QUEUE' : 'scrapy.squeues.PickleFifoDiskQueue',
        'SCHEDULER_MEMORY_QUEUE' : 'scrapy.squeues.FifoMemoryQueue',
    }
    handle_httpstatus_list = [520, 500, 502, 503, 504, 404]

    def __init__(self, urls=None, **kwargs):
        # If the user sets the "urls" argument, define it as start_urls
        if urls:
            self.start_urls = urls.split(',')
        else:
            self.start_urls = self.default_urls
        super().__init__(**kwargs)
        self.settings=get_project_settings()

    def parse(self, response):
        # Check if Amazon is sending a captcha or an incomplete page.
        captcha_input = response.xpath('//input[@id="captchacharacters"]')
        pagination = response.xpath('//*[@data-component-type="s-pagination"]')
        merchandised = response.xpath('//*[@id="merchandised-content"]')
        status_not_200 = response.status != 200
        page = response.xpath('//li[@class="a-selected"]/a/text()')
        location = response.xpath('//*[@id="glow-ingress-line2"]/text()')
        
        if (captcha_input or not pagination or merchandised or
            status_not_200 or not page or not location):
            self.logger.info('Bad response to %s. A new request will be done!' % response.url)
            request = prepare_request(response=response, callback=self.parse)
            yield request
        
        else:
            results = response.xpath('//span[@data-component-type="s-search-results"]//div[@data-asin]')
            for i, r in enumerate(results, start=1):
                url = r.xpath('.//*[@data-component-type="s-product-image"]/a/@href').extract_first()
                url = 'https://www.amazon.com%s' % url
                request = Request(response.urljoin(url), callback=self.parse_product)
                request.meta['asin'] = r.xpath('.//@data-asin').extract_first()
                request.meta['image'] = r.xpath(
                    './/*[@data-component-type="s-product-image"]//img/@src'
                ).extract_first()
                request.meta['title'] = r.xpath(
                    './/*[@data-component-type="s-product-image"]//img/@alt'
                ).extract_first()
                request.meta['price'] = r.xpath(
                    './/*[@data-a-size="l"]/span[@class="a-offscreen"]/text()'
                ).extract_first()
                request.meta['reviews'] = r.xpath(
                    './/span[@aria-label]//*[@class="a-icon-alt"]/text()'
                ).extract_first()
                request.meta['reviews_qty'] = r.xpath(
                    './/span[@aria-label]//*[@class="a-size-base"]/text()'
                ).extract_first()
                request.meta['page'] = page.extract_first()
                request.meta['referrered_url'] = response.url
                index = int(r.xpath('.//@data-index').extract_first())
                first_item = int(
                    response.xpath(
                        '//*[@data-component-type="s-result-info-bar"]//span[text()]/text()'
                    ).re(r'([0-9,]+)-')[0].replace(',', '')
                )
                request.meta['position'] = first_item + index
                request.meta['category'] = response.xpath('//meta[@name="keywords"]/@content').extract_first()
                request.meta['location'] = location.extract_first()
                yield request
        
            # Next page
            pagination = response.xpath('//*[@data-component-type="s-pagination"]')
            next_sel = pagination.xpath('.//li[@class="a-last"]/a/@href')
            if next_sel:
                next_url = next_sel.extract_first()
                next_url = 'https://www.amazon.com%s' % next_url
                yield Request(response.urljoin(next_url))
            else:
                open_in_browser(response)

    def parse_product(self, response):
        # Check if Amazon is sending a captcha or an incomplete page.
        page_title = response.xpath('//title/text()').extract_first()
        captcha_input = response.xpath('//input[@id="captchacharacters"]')
        html_elmt = response.xpath('/html[@dir="ltr"]')
        status_not_200 = response.status != 200

        if page_title == 'Robot Check' or captcha_input or html_elmt or status_not_200:
            meta_to_reload = ['asin', 'image', 'title', 'price', 'reviews', 'reviews_qty', 'page',
                              'referrered_url', 'position', 'location', 'category']
            request = prepare_request(response=response, callback=self.parse_product, meta_to_reload=meta_to_reload)
            yield request
        else:
            l = ItemLoader(item=Product(), response=response)
            l.add_value('asin', response.meta.get('asin'))
            l.add_xpath('title', str('//*[@id="productTitle"]').encode('utf-8'))
            l.add_value('_type', 'item' )
            l.add_xpath('brand', str('//*[@id="bylineInfo"]').encode('utf-8'))
            l.add_xpath('brand', str('//*[@id="brandteaser"]/img/@alt').encode('utf-8'))
            l.add_xpath('brand', str('//*[@id="mbc"]/@data-brand').encode('utf-8'))
            l.add_xpath('reviews', '//*[@id="acrPopover"]//i/span/text()', re=r'^([0-9.]+)')
            l.add_value('reviews', response.meta.get('reviews'), re=r'^([0-9.]+)')        
            l.add_xpath('reviews_qty', '//*[@id="acrCustomerReviewText"]', re=r'[0-9,]+')
            l.add_value('reviews_qty', response.meta.get('reviews_qty'), re=r'[0-9,]+')
            l.add_xpath('bsr_position', '//*[@id="SalesRank"]/text()', re=r'#([0-9,]+)')
            l.add_xpath('bsr_position',
                        '//*[contains(text(), "Best Sellers Rank")]/following::*[contains(text(), "#")][1]',
                        re=r'#([0-9,]+)')
            l.add_xpath('bsr_position', '//*[@id="SalesRank"]/td[@class="value"]', re=r'#([0-9,]+)')            
            l.add_xpath('bsr_position', '//*[@id="SalesRank"]//span[@class="zg_hrsr_rank"]', re=r'#([0-9,]+)')
            l.add_xpath('bsr_category',
                        str('//*[@id="SalesRank"]/text()').encode('utf-8'),
                        re=r'in (.*) \(')
            l.add_xpath('bsr_category',
                        '//*[contains(text(), "Best Sellers Rank")]/following::*[contains(text(), "#")][1]/text()',
                        re=r'in (.*) \(')
            l.add_xpath('bsr_category', str('//*[@id="SalesRank"]/td[@class="value"]').encode('utf-8'), re=r'in (.*) \(')  
            l.add_xpath('bsr_category', '//*[@id="SalesRank"]//a')
            l.add_value('requested_url', response.url)
            l.add_xpath('image', '//*[@id="imgTagWrapperId"]/img/@data-old-hires')
            l.add_xpath('image', '//*[@id="imgTagWrapperId"]/img/@data-a-dynamic-image', re=r'"(.+?)"')
            l.add_value('image', response.meta.get('image'))
            l.add_xpath('sold_by', '//*[@id="sns-availability"]', re=r'Sold by (.+) and')
            l.add_xpath('sold_by', '//*[@id="sns-availability"]', re=r'sold by (.+\.com)')
            l.add_xpath('sold_by', '//*[@id="merchant-info"]', re=r'Sold by (.+) and')
            l.add_xpath('sold_by', '//*[@id="merchant-info"]', re=r'sold by (.+\.com)')
            l.add_xpath('sold_by', '//*[@id="merchant-info"]', re=r'sold by (.+)\.')
            l.add_xpath('sold_by', '//*[@id="availability"]')
            l.add_xpath('price', '//*[@id="priceblock_ourprice"]')
            l.add_xpath('price', '//*[@id="priceblock_snsprice_Based"]/span[@class="a-size-large"]')
            l.add_xpath('price', '//*[@id="priceblock_saleprice"]')
            l.add_value('price', response.meta.get('price'))
            l.add_xpath('canonical_url', '//link[@rel="canonical"]/@href')
            l.add_xpath('keywords', '//meta[@name="keywords"]/@content')
            l.add_value('page', response.meta.get('page'))
            l.add_value('location', response.meta.get('location'))
            l.add_value('referrered_url', response.meta.get('referrered_url'))
            l.add_xpath('subcategories',
                        str('//*[@id="wayfinding-breadcrumbs_feature_div"]//a/text()').encode('utf-8'))
            l.add_value('position', response.meta.get('position'))
            l.add_value('category', response.meta.get('category'))

            yield l.load_item()
