# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from scrapy.utils.response import open_in_browser


def prepare_request(response, callback, meta_to_reload=None):
    """Prepares a request based on response and callback function.

    Parameters
    ----------
    response : response
        A response used to prepare the request.
    callback : function
        A function where the request will be send.
    meta_to_reload : list
        A list with meta keys to reload on request.
    Return
    ------
    request : scrapy.http.Request
        The prepared request.
    """
    request = Request(response.url, callback=callback, dont_filter=True)
    # Update attempts value
    if response.meta.get('attempts'):
        request.meta['attempts'] = int(response.meta.get('attempts')) + 1
    else:
        request.meta['attempts'] = 1

    # Load meta on request again
    for k, v in response.meta.items():
        if meta_to_reload and k in meta_to_reload:
            request.meta[k] = v
    
    return request

class ItemCounterSpider(scrapy.Spider):
    name = 'item_counter'
    allowed_domains = ['amazon.com', 'api.proxycrawl.com']
    start_urls = ['https://www.amazon.com/s?k=household+supplies&srs=5856181011&rh=n%3A5856181011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_2a1_w',
                  'https://www.amazon.com/s?i=baby&bbn=5856181011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_2b1_w',
                  'https://www.amazon.com/s?i=pets&bbn=5856181011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_2c1_w',
                  'https://www.amazon.com/s?bbn=3760911&rh=n%3A3760911%2Cp_n_is_sns_available%3A2617006011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_3c1_w',
                  'https://www.amazon.com/Medications-Treatments-Subscribe-Save-Eligible/s?i=hpc&bbn=3760941&rh=n%3A3760901%2Cn%3A%213760931%2Cn%3A3760941%2Cp_n_is_sns_available%3A2617006011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_3b1_w',
                  'https://www.amazon.com/s?i=grocery&bbn=5856181011&pf_rd_i=5856181011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a4b36cb4-0c6f-40ee-a1f3-09f0461672bd&pf_rd_r=FWPM5MYWQK3X07P70ZAM&pf_rd_s=merchandised-search-8&pf_rd_t=101&ref=s9_acss_bw_cg_snsdcw_3a1_w']
    custom_settings = {
        'FEED_EXPORT_FIELDS' : ['asin', 'category', 'subcategories', 'page', 'position', 'title', 'range', 'keywords', 
                                'requested_url', 'referrered_url'],
        'PROXYCRAWL_ENABLED' : True,
        'LOG_LEVEL' : 'INFO',
        'CONCURRENT_REQUESTS' : 64
    }
    handle_httpstatus_list = [520, 500, 502, 503, 404]

    def parse(self, response):
        # Check if Amazon is sending a captcha or an incomplete page.
        captcha_input = response.xpath('//input[@id="captchacharacters"]')
        pagination = response.xpath('//*[@data-component-type="s-pagination"]')
        merchandised = response.xpath('//*[@id="merchandised-content"]')
        status_not_200 = response.status != 200
        page = response.xpath('//li[@class="a-selected"]/a/text()')
        location = response.xpath('//*[@id="glow-ingress-line2"]/text()')
        
        if (captcha_input or not pagination or merchandised or
            status_not_200 or not page or not location):
            self.logger.info('Bad response to %s. A new request will be done!' % response.url)
            request = prepare_request(response=response, callback=self.parse)
            yield request
        
        else:
            results = response.xpath('//span[@data-component-type="s-search-results"]//div[@data-asin]')
            for i, r in enumerate(results, start = 0):
                item = {}
                item['asin'] = r.xpath('.//@data-asin').extract_first()
                item['page'] = page.extract_first()
                item['range'] = response.xpath('//*[@data-component-type="s-result-info-bar"]//span[text()]/text()').extract()
                first_item = int(
                    response.xpath(
                        '//*[@data-component-type="s-result-info-bar"]//span[text()]/text()'
                    ).re(r'([0-9,]+)-')[0].replace(',', '')
                )
                index = int(r.xpath('.//@data-index').extract_first())
                item['position'] = first_item + index
                item['category'] = response.xpath('//meta[@name="keywords"]/@content').extract_first()
                item['referrered_url'] = response.url
                item['location'] = location.extract_first()
                yield item

            # Next page
            pagination = response.xpath('//*[@data-component-type="s-pagination"]')
            next_sel = pagination.xpath('.//li[@class="a-last"]/a/@href')
            if next_sel:
                next_url = next_sel.extract_first()
                next_url = 'https://www.amazon.com%s' % next_url
                yield Request(response.urljoin(next_url))
            else:
                open_in_browser(response)
